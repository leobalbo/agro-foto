import os
from ultralytics import YOLO
import cv2

video_path_out = '{}_out.mp4'.format(r'videos\video1.mp4')

cap = cv2.VideoCapture(r'videos\video1.mp4')
ret, frame = cap.read()
H, W, _ = frame.shape
out = cv2.VideoWriter(video_path_out, cv2.VideoWriter_fourcc(*'MP4V'), int(cap.get(cv2.CAP_PROP_FPS)), (W, H))

model = YOLO(r'dataset\bestv8.pt')

threshold = 0.5
class_name_dict = {0: 'peanut'}

object_count = 0
line_position = int(H / 2)
detected_objects = {}

while ret:
    results = model(frame)[0]

    for result in results.boxes.data.tolist():
        x1, y1, x2, y2, score, class_id = result

        if score > threshold:
            cv2.rectangle(frame, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 4)
            cv2.putText(frame, class_name_dict[int(class_id)].upper(), (int(x1), int(y1 - 10)),
                        cv2.FONT_HERSHEY_SIMPLEX, 1.3, (0, 255, 0), 3, cv2.LINE_AA)

            box_center_x = int((x1 + x2) / 2)
            box_center_y = int((y1 + y2) / 2)

            cv2.circle(frame, (box_center_x, box_center_y), 3, (0, 0, 255), -1)

            if box_center_y > line_position and class_name_dict[int(class_id)] == 'peanut':
                object_id = str(class_id)
                if object_id not in detected_objects:
                    detected_objects[object_id] = True
                    object_count += 1  # incrementa o contador de objetos
                    print('Objeto cruzou a linha:', class_name_dict[int(class_id)].upper())
                    del detected_objects[object_id]  # remove o objeto rastreado

    cv2.line(frame, (0, line_position), (W, line_position), (255, 0, 0), 2)
    cv2.putText(frame, f'Quantidade de objetos: {object_count}', (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    out.write(frame)
    ret, frame = cap.read()

cap.release()
out.release()
cv2.destroyAllWindows()

print('Número total de objetos:', object_count)
